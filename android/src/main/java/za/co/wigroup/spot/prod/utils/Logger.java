package za.co.wigroup.spot.prod.utils;


import android.text.TextUtils;
import android.util.Log;

import com.meawallet.mtp.MeaError;
import com.meawallet.mtp.MeaErrorCode;
import za.co.wigroup.spot.prod.BuildConfig;


import java.util.IllegalFormatConversionException;
import java.util.Locale;
import java.util.MissingFormatArgumentException;
import java.util.UnknownFormatConversionException;

public class Logger {

    private static final String TAG = Logger.class.getSimpleName();

    private static final String SDK_TAG = "MTP-TEST-APP";
    private static final int DEBUG_MAX_LOG_SIZE = 2000;

    public static void e(String tag, String message, MeaError error, Object... args) {
        e(tag, message, error, null, args);
    }

    public static void e(String tag, String message, Object... args) {
        e(tag, message, null, null, args);
    }

    public static void e(String tag, String message, Throwable throwable) {
        e(tag, message, null, throwable);
    }

    public static void e(String tag, Throwable throwable) {
        e(tag, "", null, throwable);
    }

    public static void e(String tag, String message, MeaError error, Throwable throwable, Object... args) {
        String fullMessage = "";

        if (!TextUtils.isEmpty(message)) {
            fullMessage += getFormattedString(message, args);
        }

        if (error != null) {
            fullMessage += String.format(Locale.getDefault(), " Error message: %s, error code: %s(%d)",
                    error.getMessage(), MeaErrorCode.getName(error.getCode()), error.getCode());
        }

        if (throwable != null) {
            fullMessage += " Exception message: " + throwable.getMessage();

            String stackTraceString = Log.getStackTraceString(throwable);

            if (!TextUtils.isEmpty(stackTraceString)) {
                fullMessage += ", StackTrace:" + stackTraceString;
            }
        }

        Log.e(getTag(tag), fullMessage);
    }

    public static void w(String tag, String message, Object... args) {
        if (!BuildConfig.DEBUG) {

            return;
        }

        String fullMessage = getFormattedString(message, args);

        Log.w(getTag(tag), fullMessage);
    }

    public static void i(String tag, String message, Object... args) {
        if (!BuildConfig.DEBUG) {

            return;
        }

        Log.i(getTag(tag), getFormattedString(message, args));
    }

    public static void d(String tag, String message, Object... args) {
        if (!BuildConfig.DEBUG) {

            return;
        }

        String fullMessage = getFormattedString(message, args);

        for (int i = 0; i <= fullMessage.length() / DEBUG_MAX_LOG_SIZE; i++) {
            int start = i * DEBUG_MAX_LOG_SIZE;

            int end = (i + 1) * DEBUG_MAX_LOG_SIZE;
            end = end > fullMessage.length() ? fullMessage.length() : end;

            Log.d(getTag(tag), fullMessage.substring(start, end));
        }
    }

    public static void v(String tag, String message, Object... args) {
        if (!BuildConfig.DEBUG) {

            return;
        }

        Log.v(getTag(tag), getFormattedString(message, args));
    }

    private static String getTag(String tag) {
        return SDK_TAG + ":" + tag;
    }

    private static String getFormattedString(String inputString, Object... args) {
        try {

            return String.format(inputString, args);
        } catch (UnknownFormatConversionException | IllegalFormatConversionException | MissingFormatArgumentException ex) {
            String errorMessage = String.format("Failed to format log message exception: %s.", ex.getMessage());

            Log.e(TAG, errorMessage, ex);

            return errorMessage;
        }
    }

}