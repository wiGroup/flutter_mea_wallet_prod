package za.co.wigroup.spot.prod;

import android.nfc.NfcAdapter;

import androidx.annotation.NonNull;

import com.meawallet.mtp.MeaCard;
import com.meawallet.mtp.MeaCardListener;
import com.meawallet.mtp.MeaCheckedException;
import com.meawallet.mtp.MeaError;
import com.meawallet.mtp.MeaErrorCode;

import com.meawallet.mtp.MeaListener;
import com.meawallet.mtp.MeaTokenPlatform;
import za.co.wigroup.spot.prod.meahelpers.listeners.CardProvisionListenerImpl;
import za.co.wigroup.spot.prod.meahelpers.listeners.DigitizedCardStateChangeListenerImpl;


import io.flutter.plugin.common.MethodChannel;

public class PluginMethods {

    static void registerMEAToken(@NonNull String token, MethodChannel.Result result) {
        MeaTokenPlatform.register(token, "en", new MeaListener() {
            @Override
            public void onSuccess() {
                MeaTokenPlatform.setCardProvisionListener(new CardProvisionListenerImpl());
                MeaTokenPlatform.setDigitizedCardStateChangeListener(new DigitizedCardStateChangeListenerImpl());
                result.success(true);
            }
            @Override
            public void onFailure(@NonNull MeaError error) {
                String errorReason;
                if (error.getCode() == MeaErrorCode.ALREADY_REGISTERED) {
                    errorReason = "App instance already registered.";
                } else {
                    errorReason = "Register failed.";
                }
                result.error(errorReason + error.getCode(), error.getMessage(), null);
            }
        });
    }

    static void getDefaultCard(MethodChannel.Result result) {
        try {
            MeaCard meaCard = MeaTokenPlatform.getDefaultCardForContactlessPayments();

            if (meaCard != null) {
                result.success(true);
            } else {
                result.error("", "Failed retrieving card", "");
            }
        } catch (MeaCheckedException exception) {
            result.error("Error : " + exception.getErrorCode(), exception.getMessage(), null);
        }
    }

    static void doContactlessPayment(MethodChannel.Result result) {
        try {
            MeaCard meaCard = MeaTokenPlatform.getDefaultCardForContactlessPayments();

            if (meaCard != null) {

                meaCard.selectForContactlessPayment(new MeaCardListener() {
                    @Override
                    public void onSuccess(@NonNull MeaCard meaCard) {
                        result.success(true);
                    }

                    @Override
                    public void onFailure(@NonNull MeaError meaError) {
                        result.error("", "Failed to do contactless payment " + meaError.getMessage(), "");
                    }
                });
            } else {
                result.error("", "Failed to select card for contactless payment", "");
            }
        } catch (MeaCheckedException exception) {
            result.error("Error : " + exception.getErrorCode(), exception.getMessage(), null);
        }
    }

    static void cancelContactlessPayment(MethodChannel.Result result) {
        try {
            MeaCard meaCard = MeaTokenPlatform.getDefaultCardForContactlessPayments();

            if (meaCard != null) {
                meaCard.deselectForContactlessPayment();
                result.success(true);
            } else {
                result.error("", "Failed to deselect card for contactless payment", "");
            }
        } catch (MeaCheckedException exception) {
            result.error("Error : " + exception.getErrorCode(), exception.getMessage(), null);
        }
    }

    static void isNFCAvailable(MethodChannel.Result result) {
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(FlutterMeaWalletProdPlugin.activity);
        if (nfcAdapter == null) {
            result.success(false);
        } else {
            result.success(true);
        }
    }
}
