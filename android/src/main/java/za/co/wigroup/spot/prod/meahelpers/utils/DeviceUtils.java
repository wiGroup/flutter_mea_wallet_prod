package za.co.wigroup.spot.qa.meahelpers.utils;

import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Context;
import android.os.Build;

import za.co.wigroup.spot.prod.utils.Logger;

public abstract class DeviceUtils {

  private static final String TAG = DeviceUtils.class.getSimpleName();

  public static boolean isDeviceLocked(Context context) {
    KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);

    if (keyguardManager == null) {
      Logger.e(TAG, "Failed to check device lock screen status, keyguard manager is null");

      return false;
    }

    return keyguardManager.isKeyguardLocked();
  }

  @TargetApi(Build.VERSION_CODES.M)
  public static boolean isDeviceSecure(Context context) {
    KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);

    if (keyguardManager == null) {
      Logger.e(TAG, "Failed to check device lock screen status, keyguard manager is null");

      return false;
    }

    return keyguardManager.isDeviceSecure();
  }
}
