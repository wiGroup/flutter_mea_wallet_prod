package za.co.wigroup.spot.prod.meahelpers.intent;

import android.content.Context;

import com.meawallet.mtp.MeaContactlessTransactionData;
import za.co.wigroup.spot.prod.meahelpers.constants.IntentConstants;

public class TransactionResultIntent extends CardIdIntent {

  public static final String INTENT_DATA_IS_CARD_CALLBACK_EVENT = "INTENT_DATA_IS_CARD_CALLBACK_EVENT";

  public TransactionResultIntent(Context context,
                                 Class<?> activityClass,
                                 String action,
                                 String cardId,
                                 MeaContactlessTransactionData data,
                                 boolean isCardListenerEvent) {
    super(context, activityClass, action, cardId);

    putExtra(IntentConstants.INTENT_DATA_CARD_ID_KEY, cardId);
    putExtra(IntentConstants.INTENT_DATA_CONTACTLESS_TRANSACTION_DATA, data);
    putExtra(INTENT_DATA_IS_CARD_CALLBACK_EVENT, isCardListenerEvent);
  }

  public TransactionResultIntent setErrorCode(int errorCode) {
    putExtra(IntentConstants.INTENT_DATA_ERROR_CODE_KEY, errorCode);

    return this;
  }
}
