/*
 *  *******************************************************************************
 *            Copyright (c) 2018, MeaWallet AS. All rights reserved.
 *  *******************************************************************************
 */
package za.co.wigroup.spot.prod.meahelpers.listeners;

import android.content.Context;


import androidx.annotation.NonNull;

import com.meawallet.mtp.MeaCard;
import com.meawallet.mtp.MeaContactlessTransactionData;
import com.meawallet.mtp.MeaContactlessTransactionListener;
import com.meawallet.mtp.MeaError;
import za.co.wigroup.spot.prod.utils.Logger;

public class ContactlessTransactionListenerImpl implements MeaContactlessTransactionListener {

    private static final String TAG = ContactlessTransactionListenerImpl.class.getSimpleName();

    //TODO complete implementation if Commented codes
    private final Context mContext;
//    private final TransactionEventsHandler mTransactionEventsHandler = new TransactionEventsHandler();

    public ContactlessTransactionListenerImpl(Context context) {
        mContext = context.getApplicationContext();
    }

    @Override
    public void onContactlessPaymentStarted(MeaCard meaCard) {
        Logger.i(TAG, "onContactlessPaymentStarted(cardId = %s)", meaCard.getId());

       // mTransactionEventsHandler.handleTransactionStartedEvent(mContext, meaCard.getId(), true);
    }

    @Override
    public void onContactlessPaymentSubmitted(MeaCard meaCard, @NonNull MeaContactlessTransactionData meaContactlessTransactionData) {
        Logger.i(TAG, "onContactlessPaymentSubmitted(cardId = %s)", meaCard.getId());

       // mTransactionEventsHandler.handleTransactionSubmittedEvent(mContext, meaCard.getId(), meaContactlessTransactionData, true);
    }

    @Override
    public void onContactlessPaymentFailure(MeaCard meaCard, @NonNull MeaError error, @NonNull MeaContactlessTransactionData meaContactlessTransactionData) {
        Logger.e(TAG, "onContactlessPaymentFailure(cardId = %s)", error, meaCard.getId());

       // mTransactionEventsHandler.handleTransactionFailureEvent(mContext, meaCard.getId(), error, meaContactlessTransactionData, true);
    }

    @Override
    public void onAuthenticationRequired(MeaCard meaCard, @NonNull MeaContactlessTransactionData data) {
        Logger.i(TAG, "onAuthenticationRequired(cardId = %s, data = %s)", meaCard.getId(), data);

       // mTransactionEventsHandler.handleAuthenticationRequiredEvent(mContext, meaCard.getId(), data, true);
    }
}
