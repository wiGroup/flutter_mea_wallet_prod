#import "ProdPlugin.h"
#if __has_include(<prod/prod-Swift.h>)
#import <prod/prod-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "prod-Swift.h"
#endif

@implementation ProdPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftProdPlugin registerWithRegistrar:registrar];
}
@end
