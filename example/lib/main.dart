import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:prod/flutter_mea_wallet_prod.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Values
  String fcmToken =
      'c4IsP3D5RV6vjb-IZQyB2S:APA91bEbikTOurh4uipHZVruegKQmGCQ4_rZdyt9RSTB6Dn9XXmcv5MTZukThFZjvsWrFDFDtxclHEicetdG5xgaanrBx0y86YoRVQwWdI2Jr-Zf6Gai1O1ZoQqKbnRyewaYnCIz6tVm';
  //

  String registerRes = 'Not registered';
  String nFCRes = '';

  _button(String title, Function onPressed) {
    return RaisedButton(
      color: Color.fromARGB(255, 134, 250, 194),
      textColor: Color.fromARGB(255, 33, 28, 92),
      child: Padding(
          padding: EdgeInsets.all(16),
          child: Text(
            title,
            style: TextStyle(fontSize: 16),
          )),
      onPressed: onPressed,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side: BorderSide(color: Color.fromARGB(255, 134, 250, 194))),
    );
  }

  _registerTokenSection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _button("Register Token", () => _registerToken()),
        SizedBox(height: 10),
        Text('$registerRes')
      ],
    );
  }

  _nfcSection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _button("Check NFC", () => _isNFCAvailable()),
        SizedBox(height: 10),
        Text('$nFCRes')
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Color.fromARGB(255, 246, 246, 246),
          appBar: AppBar(
            title: const Text('MEA WALLET  | Demo app'),
            backgroundColor: Color.fromARGB(255, 55, 63, 67),
          ),
          body: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _button("Init SDK", () => _initMeaSdk()),
                _registerTokenSection(),
                _nfcSection(),
                _button("Check Default Tap To Pay App",
                    () => _checkDefaultTapToPayApp()),
                SizedBox(height: 10),
                _button("Did digitise card", () => _didDigitizeCard()),
                SizedBox(height: 10),
                _button("Deactivate card", () => _deactivateCard()),
                SizedBox(height: 10),
                _button(
                    "Do contactless payment", () => _doContactlessPayment()),
                SizedBox(height: 10),
                _button("Cancel contactless payment",
                    () => _cancelContactlessPayment()),
                SizedBox(height: 10),
                _button("Digitise Card", () => _digitizeCard()),
                SizedBox(height: 10),
                _button("Authenticate User", () => _authenticateUser()),
              ],
            ),
          )),
    );
  }

  _showMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _initMeaSdk() async {
    try {
      FlutterMeaWalletProd.initMeaSdk;
      _showMessage("Initialized");
    } on PlatformException catch (e) {
      nFCRes = e.message;
      _showMessage("Initialization Failed: ${e.message}");
    }
  }

  _isNFCAvailable() async {
    try {
      var isAvailable = await FlutterMeaWalletProd.isNFCAvailable;
      if (isAvailable) {
        nFCRes = 'NFC Available';
      } else {
        nFCRes = 'NFC Not Available';
      }
    } on PlatformException catch (e) {
      nFCRes = e.message;
    }

    setState(() {
      nFCRes = nFCRes;
    });
  }

  _registerToken() async {
    Map<String, String> MEAData = {
      'token': fcmToken,
    };

    try {
      var success = await FlutterMeaWalletProd.registerToken(MEAData);

      if (success) {
        registerRes = 'Registered token success';
      } else {
        registerRes = 'Registered token failure';
      }
    } catch (e) {
      registerRes = 'Exception on registration';
    }

    setState(() {
      registerRes = registerRes;
    });
  }

  _checkDefaultTapToPayApp() {
    try {
      FlutterMeaWalletProd.checkDefaultTapToPayApp;
    } on PlatformException catch (e) {
      print(e.message);
      throw (e.message);
    }
  }

  _didDigitizeCard() async {
    try {
      var didDigitizeCard = await FlutterMeaWalletProd.didDigitizeCard;
      if (didDigitizeCard) {
        return true;
      } else {
        return false;
      }
    } on PlatformException catch (e) {
      print(e.toString());
      throw (e.message);
    }
  }

  Future _deactivateCard() async {
    try {
      var success = await FlutterMeaWalletProd.deactivateCard;
      return success;
    } on PlatformException catch (e) {
      print(e.toString());
      throw (e.message);
    }
  }

  _doContactlessPayment() async {
    try {
      return await FlutterMeaWalletProd.doContactlessPayment;
    } on PlatformException catch (e) {
      print(e.toString());
      throw (e.message);
    }
  }

  Future _cancelContactlessPayment() async {
    try {
      return await FlutterMeaWalletProd.cancelContactlessPayment;
    } on PlatformException catch (e) {
      print(e.toString());
      throw (e.message);
    }
  }

  /*
   * var payload = {
      "accountNumber": device.pan,
      "expiryMonth": device.expiryDate.split('/')[0],
      "expiryYear": device.expiryDate.split('/')[1],
      "cardholderName": GlobalVariables.instance.subscriber.fullName
    };

    var encodedPayload = json.encode(payload);

    MethodChannel channel = MethodChannel('MEA_CHANNEL');
    Map<String, dynamic> MEAData = {
      'payload': encodedPayload,
      'cvc2': cvc2,
      'pubKeyMod': AppConfig.instance.meaPubKeyMod,
      'publicKeyFingerprint': AppConfig.instance.meaPublicKeyFingerprint,
      'pubKeyExp': AppConfig.instance.meaPubKeyExp
    };
   */
  Future<bool> _digitizeCard() async {
    Map<String, String> props = {
      'token': fcmToken,
    };
    try {
      var success = await FlutterMeaWalletProd.digitizeCard(props);
      return success;
    } on PlatformException catch (e) {
      print(e.toString());
      throw (e.message);
    }
  }

  Future _authenticateUser() async {
    try {
      return await FlutterMeaWalletProd.authenticateUser;
    } on PlatformException catch (e) {
      print(e.toString());
      throw (e.message);
    }
  }

  Future _testRemoteTransaction() async {
    try {
      return await FlutterMeaWalletProd.testRemoteTransaction;
    } on PlatformException catch (e) {
      print(e.toString());
      throw (e.message);
    }
  }
}
