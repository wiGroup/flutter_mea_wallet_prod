package za.co.wigroup.spot.prod.meahelpers.card_digitization;

import com.meawallet.mtp.MeaError;

public interface CardDigitizationListener {

  void onDigitizationInitialized(boolean isLast);

  void onDigitizationCompleted(boolean isLast);

  void onRequireAdditionalAuthentication(boolean isLast);

  void onAdditionalAuthenticationInitialized(boolean isLast);

  void onAdditionalAuthenticationCompleted(boolean isLast);

  void onDigitizationCanceled(String errorMessage, boolean isLast);

  void onFailure(String errorMessage, MeaError error, boolean isLast);
}
