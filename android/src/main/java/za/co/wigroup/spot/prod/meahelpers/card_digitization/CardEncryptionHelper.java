package za.co.wigroup.spot.prod.meahelpers.card_digitization;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;

import java.math.BigInteger;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.RSAPublicKeySpec;

public class CardEncryptionHelper {


  public static PublicKey buildRsaPublicKey(byte[] modulus, byte[] exponent) throws NoSuchAlgorithmException, InvalidKeySpecException {
    RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(1,modulus), new BigInteger(1,exponent));
    KeyFactory factory = KeyFactory.getInstance("RSA");
    return factory.generatePublic(spec);
  }

  public static SecretKey generateSecretKey(String algorithm, int size, String digestAlgorithm ) throws NoSuchAlgorithmException, NoSuchProviderException {
    SecureRandom secureRandom = new SecureRandom();
    KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithm);
    keyGenerator.init(size, secureRandom);
    return keyGenerator.generateKey();
  }

  public static IvParameterSpec generateIv() throws NoSuchAlgorithmException, NoSuchProviderException {
    SecureRandom randomSecureRandom = SecureRandom.getInstance("SHA1PRNG");
    byte[] ivBytes = new byte[16]; //cipher.getBlockSize()
    randomSecureRandom.nextBytes(ivBytes);
    return  new IvParameterSpec(ivBytes);
  }

  public static byte[] crypt(int operation, String algorithm, String provider, Key key, AlgorithmParameterSpec iv, byte[] clearText)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
    InvalidAlgorithmParameterException, BadPaddingException, IllegalBlockSizeException,
    NoSuchProviderException {

    if (operation == Cipher.UNWRAP_MODE || operation == Cipher.WRAP_MODE) {
      throw new InvalidAlgorithmParameterException("Cannot use Wrap/UnWrap in a crypt method");
    }

    Cipher currentCipher = null;

    if (provider != null) {
      currentCipher = Cipher.getInstance(algorithm, provider);
    } else {
      currentCipher = Cipher.getInstance(algorithm);
    }

    if (iv == null) {
      currentCipher.init(operation, key);
    } else {
      currentCipher.init(operation, key, iv);
    }

    return currentCipher.doFinal(clearText);
  }

  public static byte[] wrap(String algorithm, String provider, Key publicKey, Key secretKey)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
    InvalidAlgorithmParameterException, BadPaddingException, IllegalBlockSizeException,
    NoSuchProviderException {

    Cipher currentCipher = null;

    if (provider != null) {
      currentCipher = Cipher.getInstance(algorithm, provider);
    } else {
      currentCipher = Cipher.getInstance(algorithm);
    }

    if (algorithm.contains("OAEPWith")) {

      OAEPParameterSpec sp = new OAEPParameterSpec("SHA-512", "MGF1", new MGF1ParameterSpec("SHA-512"), PSource.PSpecified.DEFAULT);
      currentCipher.init(Cipher.WRAP_MODE, publicKey, sp);
    } else {
      currentCipher.init(Cipher.WRAP_MODE, publicKey);
    }
    return currentCipher.wrap(secretKey);
  }

  private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
  public static String bytesToHex(byte[] bytes) {
    char[] hexChars = new char[bytes.length * 2];
    for (int j = 0; j < bytes.length; j++) {
      int v = bytes[j] & 0xFF;
      hexChars[j * 2] = HEX_ARRAY[v >>> 4];
      hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
    }
    return new String(hexChars);
  }

  public static byte[] hexStringToByteArray(String s) {
    int len = s.length();
    byte[] data = new byte[len / 2];
    for (int i = 0; i < len; i += 2) {
      data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
        + Character.digit(s.charAt(i+1), 16));
    }
    return data;
  }
}
