package za.co.wigroup.spot.prod.meahelpers.interfaces;

/*
 *  *******************************************************************************
 *            Copyright (c) 2018, MeaWallet AS. All rights reserved.
 *  *******************************************************************************
 */

import android.app.Activity;
import android.content.Context;

import za.co.wigroup.spot.prod.meahelpers.SharedPreferencesManager;

public interface BaseActivityInterface {

  Context getContext();

  Activity getActivity();

  SharedPreferencesManager getSharedPreferenceManager();

  String getString(int stringResourceId);
}
