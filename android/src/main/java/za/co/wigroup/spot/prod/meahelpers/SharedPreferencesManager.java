package za.co.wigroup.spot.prod.meahelpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import za.co.wigroup.spot.prod.BuildConfig;
import za.co.wigroup.spot.prod.utils.Logger;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

public class SharedPreferencesManager {

  private static final String TAG = SharedPreferencesManager.class.getSimpleName();

  private Gson mGson;

  private static final String DO_NOT_ASK_PERMISSIONS_KEY = BuildConfig.APPLICATION_ID + ".DO_NOT_ASK_PERMISSIONS";
  private static final String CARDS_DTO_KEY = BuildConfig.APPLICATION_ID + ".CARDS_DTO";
  private static final String CARD_LISTENERS_ENABLED_KEY = BuildConfig.APPLICATION_ID + ".CARD_LISTENERS_ENABLED_KEY";

  private final Context mContext;

  private SharedPreferences mSharedPreferences;

  public SharedPreferencesManager(Context context) {
    mContext = context;
  }

  private SharedPreferences getSharedPreferences() {
    if (mSharedPreferences == null) {
      mSharedPreferences = mContext.getSharedPreferences(mContext.getPackageName(), Context.MODE_PRIVATE);
    }

    return mSharedPreferences;
  }

  public boolean isDontAskPermissionsSet() {
    return getSharedPreferences().getBoolean(DO_NOT_ASK_PERMISSIONS_KEY, false);
  }

  public boolean setDontAskPermissions(boolean value) {
    return savePreference(DO_NOT_ASK_PERMISSIONS_KEY, value);
  }

  public boolean isTransactionEventsCardListenersEnabled() {
    return getSharedPreferences().getBoolean(CARD_LISTENERS_ENABLED_KEY, false);
  }

  public boolean setTransactionEventsCardListenersEnabled(boolean value) {
    return savePreference(CARD_LISTENERS_ENABLED_KEY, value);
  }

  private Gson getGson() {
    if (mGson == null) {
      mGson = new Gson();
    }

    return mGson;
  }

  private ArrayList<CardDto> mCardDtoArrayList;

  private ArrayList<CardDto> getCardDtoArrayList() {

    if (mCardDtoArrayList == null) {
      String cardDtoArrayListString = getSharedPreferences().getString(CARDS_DTO_KEY, "");

      if (!TextUtils.isEmpty(cardDtoArrayListString)) {

        Type type = new TypeToken<ArrayList<CardDto>>() {
        }.getType();

        mCardDtoArrayList = getGson().fromJson(cardDtoArrayListString, type);
      } else {
        mCardDtoArrayList = new ArrayList<>();
      }
    }

    return mCardDtoArrayList;
  }

  private boolean cardDtoExists(CardDto newCardDto) {
    for (CardDto cardDto : getCardDtoArrayList()) {

      if (cardDto.getEligibilityReceipt().equals(newCardDto.getEligibilityReceipt()) ||
        (newCardDto.getDigitizedCardId() != null && cardDto.getDigitizedCardId().equals(newCardDto.getDigitizedCardId()))) {

        return true;
      }
    }

    return false;
  }

  public boolean wipeAllCardDtos() {
    return savePreference(CARDS_DTO_KEY, "");
  }

  public boolean wipeCardDtoById(String cardId) {
    ArrayList<CardDto> cardDtoArrayList = getCardDtoArrayList();

    for (CardDto cardDto : cardDtoArrayList) {

      if (cardDto.getDigitizedCardId().equalsIgnoreCase(cardId)) {
        cardDtoArrayList.remove(cardDto);

        return savePreference(CARDS_DTO_KEY, getGson().toJson(cardDtoArrayList));
      }
    }

    return true;
  }

  public boolean wipeCardDtoByEligibilityReceipt(String eligibilityReceipt) {
    ArrayList<CardDto> cardDtoArrayList = getCardDtoArrayList();

    for (CardDto cardDto : cardDtoArrayList) {
      if (cardDto.getEligibilityReceipt().equalsIgnoreCase(eligibilityReceipt)) {
        cardDtoArrayList.remove(cardDto);

        return savePreference(CARDS_DTO_KEY, getGson().toJson(cardDtoArrayList));
      }
    }

    return true;
  }

  public void clearCardDtoArrayList() {
    mCardDtoArrayList = null;
  }

  public boolean setCardDto(CardDto cardDto) {
    if (cardDtoExists(cardDto)) {
      return updateCardDto(cardDto);
    } else {
      getCardDtoArrayList().add(cardDto);

      return savePreference(CARDS_DTO_KEY, getGson().toJson(getCardDtoArrayList()));
    }
  }

  public boolean updateCardDto(CardDto newCardDto) {
    ArrayList<CardDto> cardDtoArrayList = getCardDtoArrayList();

    for (CardDto existingCardDto : cardDtoArrayList) {

      if ((existingCardDto.getEligibilityReceipt() != null &&
        existingCardDto.getEligibilityReceipt().equalsIgnoreCase(newCardDto.getEligibilityReceipt())) ||
        (existingCardDto.getDigitizedCardId() != null &&
          existingCardDto.getDigitizedCardId().equals(newCardDto.getDigitizedCardId()))) {

        cardDtoArrayList.remove(existingCardDto);
        cardDtoArrayList.add(newCardDto);

        return savePreference(CARDS_DTO_KEY, getGson().toJson(cardDtoArrayList));
      }
    }

    return true;
  }

  public ArrayList<CardDto> getSortedCardDtoArrayByDigitizationTime() {
    ArrayList<CardDto> cardDtoArray = getCardDtoArrayList();

    Collections.sort(cardDtoArray, (CardDto card1, CardDto card2) ->
      Long.compare(card1.getCardDigitizationTime(), card2.getCardDigitizationTime())
    );

    return cardDtoArray;
  }

  public CardDto getCardDtoWithEligibilityReceipt(String eligibilityReceipt) {
    for (CardDto cardDto : getCardDtoArrayList()) {
      if (cardDto.getEligibilityReceipt().equals(eligibilityReceipt)) return cardDto;
    }

    return null;
  }

  private boolean savePreference(String key, String value) {
    SharedPreferences.Editor editor = getSharedPreferences().edit();
    editor.putString(key, value);

    boolean success = editor.commit();

    if (!success) {
      Logger.e(TAG, "Error during saving %s with value %s", key, value);
    }

    return success;
  }

  private boolean savePreference(String key, Boolean value) {
    SharedPreferences.Editor editor = getSharedPreferences().edit();
    editor.putBoolean(key, value);

    boolean success = editor.commit();

    if (!success) {
      Logger.e(TAG, "Error during saving %s with value %b", key, value);
    }

    return success;
  }

  private boolean savePreference(String key, Long value) {
    SharedPreferences.Editor editor = getSharedPreferences().edit();
    editor.putLong(key, value);

    boolean success = editor.commit();

    if (!success) {
      Logger.e(TAG, "Error during saving %s with value %d", key, value);
    }

    return success;
  }

  public void wipe() {
    getSharedPreferences().edit().clear().apply();
  }
}
