package za.co.wigroup.spot.prod.meahelpers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.os.Build;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import za.co.wigroup.spot.prod.R;
import za.co.wigroup.spot.prod.utils.Logger;

/**
 * Helper class used to manage notification channels, and create/show notifications.
 */
public class NotificationHelper extends ContextWrapper {

  private static final String LOG_TAG = NotificationHelper.class.getSimpleName();
  private static final String DEFAULT_CHANNEL = "MEA_DEFAULT_CHANNEL";
  private static final long[] DEFAULT_VIBRATE_PATTERN = {0, 250, 250, 250};

  private NotificationManager mNotificationManager;

  /**
   * Registers notification channel for Android Oreo ang higher.
   */
  public NotificationHelper(Context context) {
    super(context);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      NotificationChannel defaultChannel = new NotificationChannel(DEFAULT_CHANNEL,
        getString(R.string.app_name),
        NotificationManager.IMPORTANCE_DEFAULT);
      defaultChannel.setLightColor(Color.WHITE);
      defaultChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

      NotificationManager notificationManager = getNotificationManager();

      if (notificationManager != null) {
        notificationManager.createNotificationChannel(defaultChannel);
      }
    }
  }

  /**
   * Sets up general Notification Builder which can be modified later.
   */
  public NotificationCompat.Builder getNotification(String message, PendingIntent pendingIntent) {
    return getNotification(message, getString(R.string.app_name), pendingIntent);
  }

  public NotificationCompat.Builder getNotification(String message, String title, PendingIntent pendingIntent) {
    NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), DEFAULT_CHANNEL)
      .setSmallIcon(R.mipmap.ic_notification)                // Notification icon
      .setContentTitle(title)                                 // Title for notification
      .setContentText(message)                                // Message for notification
      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)    // Notification visibility flag
      .setAutoCancel(true)                                    // Clear notification after click
      .setContentIntent(pendingIntent)
      .setVibrate(DEFAULT_VIBRATE_PATTERN)
      .setLights(Color.WHITE, 2000, 3000)
      .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      builder.setCategory(Notification.CATEGORY_MESSAGE);
    }

    return builder;
  }

  /**
   * Sends a notification.
   *
   * @return true if notifications shown successfully
   */
  public boolean notify(@SuppressWarnings("SameParameterValue") int id, NotificationCompat.Builder notification) {
    NotificationManager notificationManager = getNotificationManager();

    if (notificationManager != null) {
      notificationManager.notify(id, notification.build());

      return true;
    }

    return false;
  }

  @Nullable
  private NotificationManager getNotificationManager() {
    if (mNotificationManager == null) {
      mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

      if (mNotificationManager == null) {

        Logger.e(LOG_TAG, "Notification manager is null, notifications will not be shown.");
      }
    }

    return mNotificationManager;
  }
}
