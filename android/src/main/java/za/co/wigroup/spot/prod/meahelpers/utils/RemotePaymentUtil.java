package za.co.wigroup.spot.prod.meahelpers.utils;

import com.meawallet.mtp.MeaRemotePaymentData;
import com.meawallet.mtp.RemoteCryptogramType;

import java.util.Date;

public class RemotePaymentUtil {

  @SuppressWarnings("SameParameterValue")
  public static MeaRemotePaymentData getSampleMeaRemotePaymentData(final long amount, final int currency) {
    return new MeaRemotePaymentData() {
      @Override
      public long getTransactionAmount() {
        return amount;
      }

      @Override
      public int getCurrencyCode() {
        return currency;
      }

      @Override
      public Integer getCountryCode() {
        return 826;
      }

      @Override
      public byte getTransactionType() {
        return (byte) 1;
      }

      @Override
      public RemoteCryptogramType getCryptogramType() {
        return RemoteCryptogramType.DE55;
      }

      @Override
      public int getOptionalUnpredictableNumber() {
        return 12345;
      }

      @Override
      public Date getTransactionDate() {
        return new Date();
      }
    };
  }
}
