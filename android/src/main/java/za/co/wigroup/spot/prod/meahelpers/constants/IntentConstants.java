/*
 *  *******************************************************************************
 *            Copyright (c) 2018, MeaWallet AS. All rights reserved.
 *  *******************************************************************************
 */

package za.co.wigroup.spot.prod.meahelpers.constants;

public interface IntentConstants {

  String INTENT_ACTION = "action";
  String INTENT_START_OVER_LOCK_SCREEN = "start_over_lock_screen";
  String INTENT_ACTION_AUTHENTICATION_REQUIRED = "authentication_required";
  String INTENT_ACTION_TRANSACTION_SUBMITTED = "transaction_submitted";
  String INTENT_ACTION_TRANSACTION_FAILURE = "transaction_failure";
  String INTENT_ACTION_TRANSACTION_STARTED = "transaction_started";
  String INTENT_ACTION_WALLET_PIN_RESET_REQUEST = "wallet_pin_reset";
  String INTENT_ACTION_CARD_PIN_RESET_REQUEST = "card_pin_reset";
  String INTENT_ACTION_REMOTE_PAYMENT = "remote_payment";
  String INTENT_ACTION_TRANSACTION_PUSH = "transaction_push";
  String INTENT_ACTION_DECRYPTED_DATA = "decrypted_data";
  String INTENT_ACTION_SKIP_REGISTRATION = "skip_registration";

  String INTENT_DATA_CARD_ID_KEY = "card_id";
  String INTENT_DATA_CONTACTLESS_TRANSACTION_DATA = "contactless_transaction_data";
  String INTENT_DATA_ERROR_CODE_KEY = "error_code";
}
