package za.co.wigroup.spot.prod.meahelpers.receivers;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.meawallet.mtp.MeaCheckedException;
import com.meawallet.mtp.MeaTokenPlatform;
import com.meawallet.mtp.MeaTransactionAuthorizationStatus;
import com.meawallet.mtp.MeaTransactionMessage;
import za.co.wigroup.spot.prod.FlutterMeaWalletProdPlugin;
import za.co.wigroup.spot.prod.meahelpers.NotificationHelper;
import za.co.wigroup.spot.prod.meahelpers.constants.IntentConstants;
import za.co.wigroup.spot.prod.meahelpers.intent.AppIntent;
import za.co.wigroup.spot.prod.utils.Logger;

import java.util.Map;

public class MEAFcmListenerService extends FirebaseMessagingService {

  private static final String TAG =  MEAFcmListenerService.class.getSimpleName();
  private static NotificationHelper sNotificationHelper;
  private static final int NOTIFICATION_ID = 1;

  @Override
  public void onMessageReceived(final RemoteMessage message) {

    Map<String, String> messageData = message.getData();
    Logger.d(TAG, "onMessageReceived(): from: %s, data: %s", message.getFrom(), messageData);

    if (!MeaTokenPlatform.Rns.isMeaRemoteMessage(messageData)) {
      return;
    }

    try{
      if (MeaTokenPlatform.Rns.isMeaRemoteMessage(messageData)) {
        if (MeaTokenPlatform.Rns.isMeaTransactionMessage(messageData)) {

          MeaTransactionMessage transactionMessage =
            MeaTokenPlatform.Rns.parseTransactionMessage(messageData);

          MeaTransactionAuthorizationStatus authorizationStatus =
            transactionMessage != null ? transactionMessage.getAuthorizationStatus() : null;

          // Ignore if transaction status is CLEARED
          if (authorizationStatus == MeaTransactionAuthorizationStatus.CLEARED) {

            return;
          }

          Double amount = (transactionMessage != null) ? (transactionMessage.getAmount()) : (null);
          String currencyCode = (transactionMessage != null) ? (transactionMessage.getCurrencyCode()) : ("");
          String merchantName = (transactionMessage != null) ? (transactionMessage.getMerchantName()) : ("");

          StringBuilder notificationText = new StringBuilder()
            .append(amount)
            .append(" ")
            .append(currencyCode);

          if (authorizationStatus == MeaTransactionAuthorizationStatus.DECLINED
            || authorizationStatus == MeaTransactionAuthorizationStatus.REVERSED) {

            notificationText.append(" (").append(authorizationStatus.toString()).append(")");
          }

          showTransactionNotification(getApplicationContext(), notificationText.toString(), merchantName);

        } else {
          MeaTokenPlatform.Rns.onMessageReceived(messageData);
          Logger.d(TAG, "onMessageReceived success.");
        }
      }
    }catch (MeaCheckedException exception){
      Logger.e(TAG, "Failed to process PUSH message.", exception);
    }
  }

  private void showTransactionNotification(Context context, String text, String title) {
    Intent cardActivityIntent = new AppIntent(context, FlutterMeaWalletProdPlugin.class, IntentConstants.INTENT_ACTION_TRANSACTION_PUSH);

    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, cardActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    NotificationCompat.Builder notification = getNotificationHelper(context).getNotification(text, title, pendingIntent);

    getNotificationHelper(context).notify(NOTIFICATION_ID, notification);
  }

  private static NotificationHelper getNotificationHelper(Context context) {
    if (sNotificationHelper == null) {
      sNotificationHelper = new NotificationHelper(context);
    }
    return sNotificationHelper;
  }
}
