package za.co.wigroup.spot.prod.meahelpers.card_digitization;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.meawallet.mtp.MeaCard;
import com.meawallet.mtp.MeaCardState;
import com.meawallet.mtp.MeaCheckedException;
import com.meawallet.mtp.MeaCompleteDigitizationListener;
import com.meawallet.mtp.MeaError;
import com.meawallet.mtp.MeaTokenPlatform;
import com.meawallet.mtp.PaymentNetwork;
import za.co.wigroup.spot.prod.meahelpers.CardDto;
import za.co.wigroup.spot.prod.meahelpers.SharedPreferencesManager;
import za.co.wigroup.spot.prod.meahelpers.interfaces.BaseActivityInterface;
import za.co.wigroup.spot.prod.utils.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import io.flutter.plugin.common.MethodChannel;


public class CardDigitizationHelper {

  private static final String TAG = CardDigitizationHelper.class.getSimpleName();

  private final BaseActivityInterface mBaseActivityInterface;
  private List<CardDigitizationListener> mCardDigitizationListeners = new ArrayList<>();
  private CardDigitizationListener mMultipleCardDigitizationListener;
  private int mCardCountToDigitize;

  public CardDigitizationHelper(BaseActivityInterface baseActivityInterface) {
    mBaseActivityInterface = baseActivityInterface;
  }

  private SharedPreferencesManager getSharedPreferencesManager() {
    return mBaseActivityInterface.getSharedPreferenceManager();
  }

  private List<MeaCard> getCardsListByState(MeaCardState... states) {
    List<MeaCard> resultCards = new ArrayList<>();

    try {
      List<MeaCard> cards = MeaTokenPlatform.getCards();

      for (MeaCard card : cards) {

        for (MeaCardState state : states) {

          if (card.getState() == state) {
            resultCards.add(card);

            break;
          }
        }
      }
    } catch (MeaCheckedException exception) {
      Logger.e(TAG, String.valueOf(exception));
    }

    return resultCards;
  }

  private boolean checkCardsForCompleteDigitizationSize(String cvc2, List<MeaCard> cardArrayList, CardDigitizationListener cardDigitizationListener) {
    Logger.d(TAG, "checkCardsForCompleteDigitizationSize()");

    int size = cardArrayList.size();

    Logger.d(TAG, "cardArrayList.size() = %d", size);

    if (size == 0) {
      Logger.i(TAG,"There is no cards to complete digitization.");
      return false;
    }

    if (size == 1) {
      String eligibilityReceipt = cardArrayList.get(0).getEligibilityReceipt();

      CardDto cardDto = getSharedPreferencesManager().getCardDtoWithEligibilityReceipt(eligibilityReceipt);

      handlePaymentNetworkAndSecurityCodeCheck(cvc2, cardDto, false, true, cardDigitizationListener);

      return false;
    }

    return true;
  }

  private void handlePaymentNetworkAndSecurityCodeCheck(final String cvc2,
                                                        final CardDto cardDto,
                                                        final boolean autoAcceptTermsAndConditions,
                                                        final boolean isLastCard, CardDigitizationListener cardDigitizationListener) {
    Logger.d(TAG, "handlePaymentNetworkAndSecurityCodeCheck()");

    boolean isSecurityCodeApplicable = cardDto.getSecurityCodeApplicable();

    if (isSecurityCodeApplicable && TextUtils.isEmpty(cvc2)) {
      sendCallback(CallbackMethod.FAILURE, "Missing CVC code.", null, isLastCard);

      return;
    }
    // Skip terms and conditions approve prompt in case if autoAcceptTermsAndConditions = true or card is AMEX card
    if (autoAcceptTermsAndConditions || cardDto.getPaymentNetwork().equalsIgnoreCase(PaymentNetwork.AMEX.name())) {
      addCardDigitizationListener(cardDigitizationListener);
      handleCompleteDigitization(cvc2, cardDto, isLastCard);

      return;
    }
  }

  private void handleCompleteDigitization(String cvc2, final CardDto cardDto, final boolean isLastCard) {
    Logger.d(TAG, "handleCompleteDigitization()");

    String eligibilityReceiptValue = cardDto.getEligibilityReceipt();
    String termsAndConditionsAssetId = cardDto.getTermsAndConditionsAssetId();

    // Progress dialog is shown in handleCheckPaymentNetworkSecurityCodeAndFetchTermsAndConditionsAsset()
    MeaTokenPlatform.completeDigitization(
      eligibilityReceiptValue,
      termsAndConditionsAssetId,
      System.currentTimeMillis(),
      cvc2,
      new MeaCompleteDigitizationListener() {

        @Override
        public void onSuccess(@NonNull MeaCard card) {

          getSharedPreferencesManager().updateCardDto(cardDto.setDigitizedCardId(card.getId()));

          Logger.d(TAG, "Card id = %s", card.getId());

//          if (isLastCard) {
//            getProgressDialogHelper().dismissProgress();
//          }

          sendCallback(CallbackMethod.DIGITIZATION_COMPLETED, isLastCard);
        }

        @Override
        public void onRequireAdditionalAuthentication(@NonNull MeaCard card) {

          getSharedPreferencesManager().updateCardDto(cardDto.setDigitizedCardId(card.getId()));

          Logger.d(TAG, "Card id = %s", card.getId());

//          if (isLastCard) {
//            getProgressDialogHelper().dismissProgress();
//          }

          sendCallback(CallbackMethod.REQUIRED_ADDITIONAL_AUTHENTICATION, isLastCard);
        }

        @Override
        public void onFailure(@NonNull MeaError error) {
          Logger.e(TAG, "Failed to complete digitization.", error);

//          getProgressDialogHelper().dismissProgress();

          sendCallback(CallbackMethod.FAILURE, "Failed to complete digitization.", error, isLastCard);
        }
      });
  }


  public synchronized void addCardDigitizationListener(CardDigitizationListener listener) {
    Logger.d(TAG, "addCardDigitizationListener()");

    if (!mCardDigitizationListeners.contains(listener)) {

      mCardDigitizationListeners.add(listener);
    }
  }

  private void sendCallback(CallbackMethod method, boolean isLast) {
    sendCallback(method, null, null, isLast);
  }

  enum CallbackMethod {
    DIGITIZATION_INITIALIZED,
    DIGITIZATION_COMPLETED,
    REQUIRED_ADDITIONAL_AUTHENTICATION,
    ADDITIONAL_AUTHENTICATION_INITIALIZED,
    ADDITIONAL_AUTHENTICATION_COMPLETED,
    CANCELED,
    FAILURE
  }

  private void sendCallback(CallbackMethod method, String errorMessage, MeaError error, boolean isLast) {
    CopyOnWriteArrayList<CardDigitizationListener> listenersToExecute = new CopyOnWriteArrayList<>(mCardDigitizationListeners);

    //noinspection ForLoopReplaceableByForEach
    for (Iterator<CardDigitizationListener> iterator = listenersToExecute.iterator(); iterator.hasNext(); ) {
      CardDigitizationListener listener = iterator.next();

      switch (method) {

        case DIGITIZATION_INITIALIZED:
          listener.onDigitizationInitialized(isLast);
          break;

        case DIGITIZATION_COMPLETED:
          listener.onDigitizationCompleted(isLast);
          break;

        case REQUIRED_ADDITIONAL_AUTHENTICATION:
          listener.onRequireAdditionalAuthentication(isLast);
          break;

        case ADDITIONAL_AUTHENTICATION_INITIALIZED:
          listener.onAdditionalAuthenticationInitialized(isLast);
          break;

        case ADDITIONAL_AUTHENTICATION_COMPLETED:
          listener.onAdditionalAuthenticationCompleted(isLast);
          break;

        case CANCELED:
          listener.onDigitizationCanceled(errorMessage, isLast);
          break;

        case FAILURE:
          listener.onFailure(errorMessage, error, isLast);
          break;
      }
    }
  }

  void completeDigitizationForOneCard(String cvc2, boolean autoAcceptTermsAndConditions, CardDigitizationListener cardDigitizationListener, MethodChannel.Result result){
    Logger.d("MEA", "completeDigitizationForOneCard(cvc2 = %s)", cvc2);

    List<MeaCard> cardList = getCardsListByState(MeaCardState.DIGITIZATION_STARTED);

    if (checkCardsForCompleteDigitizationSize(cvc2, cardList, cardDigitizationListener)) {

//      getAlertDialogHelper().showCardChooseDialog("Choose cards to complete digitization", cardList, meaCard -> {
//        String eligibilityReceipt = meaCard.getEligibilityReceipt();
//        CardDto cardDto = getSharedPreferencesManager().getCardDtoWithEligibilityReceipt(eligibilityReceipt);
//
//        handlePaymentNetworkAndSecurityCodeCheck(cvc2, cardDto, autoAcceptTermsAndConditions, true, cardDigitizationListener);
//      });

    }
  }
}
