package za.co.wigroup.spot.prod.meahelpers.intent;

import android.content.Context;
import android.content.Intent;

import za.co.wigroup.spot.prod.meahelpers.constants.IntentConstants;

public class AppIntent extends Intent implements IntentConstants {

  public AppIntent(Context context, Class<?> activityClass, String action) {
    super(context, activityClass);

    putExtra(INTENT_ACTION, action);
  }

  public Intent setDisplayOverLockScreen() {
    setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    putExtra(INTENT_START_OVER_LOCK_SCREEN, 1);

    return this;
  }
}
