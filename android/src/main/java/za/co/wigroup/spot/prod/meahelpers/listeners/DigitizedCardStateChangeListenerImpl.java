package za.co.wigroup.spot.prod.meahelpers.listeners;

import androidx.annotation.NonNull;

import com.meawallet.mtp.MeaCard;
import com.meawallet.mtp.MeaCardState;
import com.meawallet.mtp.MeaDigitizedCardStateChangeListener;
import za.co.wigroup.spot.prod.FlutterMeaWalletProdPlugin;
import za.co.wigroup.spot.prod.utils.Logger;

public class DigitizedCardStateChangeListenerImpl implements MeaDigitizedCardStateChangeListener {

  private static final String TAG = DigitizedCardStateChangeListenerImpl.class.getSimpleName();

  @Override
  public void onStateChanged(MeaCard meaCard, @NonNull MeaCardState meaCardState) {
    Logger.d(TAG, "onStateChanged(cardId = %s, meaCardState = %s)", meaCard.getId(), meaCardState);
    if (meaCardState.equals(MeaCardState.ACTIVE)) {
      FlutterMeaWalletProdPlugin.sendMessageToFlutter("cardState", "", FlutterMeaWalletProdPlugin.CARD_ACTIVATION_COMPLETED, null);
    }
  }
}
