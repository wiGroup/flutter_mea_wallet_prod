/*
 *  *******************************************************************************
 *            Copyright (c) 2018, MeaWallet AS. All rights reserved.
 *  *******************************************************************************
 */

package za.co.wigroup.spot.prod.meahelpers.listeners;


import android.util.Log;

import androidx.annotation.NonNull;

import com.meawallet.mtp.MeaCard;
import com.meawallet.mtp.MeaCardProvisionListener;
import com.meawallet.mtp.MeaCheckedException;
import com.meawallet.mtp.MeaError;
import com.meawallet.mtp.MeaTokenInfo;

public class CardProvisionListenerImpl implements MeaCardProvisionListener {

  @Override
  public void onCardProvisionCompleted(MeaCard card) {
    try {
      MeaTokenInfo tokenInfo = card.getTokenInfo();

      String panSuffix = "";

      if (tokenInfo != null) {
        panSuffix = tokenInfo.getTokenPanSuffix();
      }

      Log.d("MainActivity", "Card ***-" + panSuffix + " is provisioned and ready for use.");
    } catch (MeaCheckedException exception) {

      Log.d("MainActivity", "Card provision failed. " + exception.toString());
    }
  }

  @Override
  public void onCardProvisionFailure(@NonNull MeaCard card, @NonNull MeaError error) {
    Log.d("MainActivity", "Card provision failed. " + card + error);
  }
}
