package za.co.wigroup.spot.prod.meahelpers;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.meawallet.mtp.MeaContactlessTransactionData;
import com.meawallet.mtp.MeaError;
import com.meawallet.mtp.MeaTokenPlatform;
import za.co.wigroup.spot.prod.FlutterMeaWalletProdPlugin;
import za.co.wigroup.spot.prod.meahelpers.constants.IntentConstants;
import za.co.wigroup.spot.prod.meahelpers.intent.TransactionResultIntent;

import za.co.wigroup.spot.qa.meahelpers.utils.DeviceUtils;

public class TransactionEventsHandler implements IntentConstants {

  private static NotificationHelper sNotificationHelper;

  public void handleTransactionStartedEvent(Context context,
                                            String cardId,
                                            boolean isCardListenerEvent) {

    if (!MeaTokenPlatform.Configuration.isSaveAuthWhenLocked() && DeviceUtils.isDeviceLocked(context)) {
      return;
    }

    TransactionResultIntent cardActivityIntent = new TransactionResultIntent(
      context, FlutterMeaWalletProdPlugin.class,
      INTENT_ACTION_TRANSACTION_STARTED,
      cardId,
      null,
      isCardListenerEvent);

    context.startActivity(cardActivityIntent.setDisplayOverLockScreen());
  }

  public void handleTransactionSubmittedEvent(Context context,
                                              String cardId,
                                              MeaContactlessTransactionData data,
                                              boolean isCardListenerEvent) {
    TransactionResultIntent cardActivityIntent = new TransactionResultIntent(
      context, FlutterMeaWalletProdPlugin.class,
      INTENT_ACTION_TRANSACTION_SUBMITTED,
      cardId,
      data,
      isCardListenerEvent);

    String notificationMessage = String.format("Transaction submitted. Amount: %1$s Currency: %2$s", data.getAmount(), data.getCurrency());

    if (showLockScreenNotification(context, cardActivityIntent, notificationMessage)) {

      return;
    }

    // Open Activity if Notification was not shown
    context.startActivity(cardActivityIntent.setDisplayOverLockScreen());
  }

  public void handleTransactionFailureEvent(Context context,
                                            String cardId,
                                            MeaError error,
                                            MeaContactlessTransactionData data,
                                            boolean isCardListenerEvent) {
    TransactionResultIntent cardActivityIntent = new TransactionResultIntent(
      context, FlutterMeaWalletProdPlugin.class,
      INTENT_ACTION_TRANSACTION_FAILURE,
      cardId,
      data,
      isCardListenerEvent)
      .setErrorCode(error.getCode());

    String notificationMessage = "Transaction failed - " + error.getCode();

    if (showLockScreenNotification(context, cardActivityIntent, notificationMessage)) {

      return;
    }

    // Open Activity if Notification was not shown
    context.startActivity(cardActivityIntent.setDisplayOverLockScreen());
  }

  public void handleAuthenticationRequiredEvent(Context context, String cardId, MeaContactlessTransactionData data, boolean isCardListenerEvent) {
    TransactionResultIntent transactionResultIntent = new TransactionResultIntent(
      context, FlutterMeaWalletProdPlugin.class,
      INTENT_ACTION_AUTHENTICATION_REQUIRED,
      cardId,
      data,
      isCardListenerEvent);

    // No need to show Notification as starting from Android Oreo KeyguardManager.requestDismissKeyguard() will unlock lock screen
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
      String notificationMessage = String.format("Open to pay %1$s %2$s", data.getAmount(), data.getCurrency());

      if (showLockScreenNotification(context, transactionResultIntent, notificationMessage)) {

        return;
      }
    }
    // Open Activity if Notification was not shown
    context.startActivity(transactionResultIntent.setDisplayOverLockScreen());
  }

  /**
   * @return true if Notification was shown
   */
  private static boolean showLockScreenNotification(Context context, Intent activityIntent, String notificationMessage) {
    if (!MeaTokenPlatform.Configuration.isSaveAuthWhenLocked() && DeviceUtils.isDeviceLocked(context)) {

      PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
      NotificationCompat.Builder notification = getNotificationHelper(context).getNotification(notificationMessage, pendingIntent);

      return getNotificationHelper(context).notify(0, notification);
    }

    return false;
  }

  private static NotificationHelper getNotificationHelper(Context context) {
    if (sNotificationHelper == null) {
      sNotificationHelper = new NotificationHelper(context);
    }

    return sNotificationHelper;
  }
}
