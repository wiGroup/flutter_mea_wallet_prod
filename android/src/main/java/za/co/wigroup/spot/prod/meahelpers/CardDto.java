package za.co.wigroup.spot.prod.meahelpers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardDto {

  @Expose
  @SerializedName("eligibility_receipt")
  private String mEligibilityReceipt;

  @Expose
  @SerializedName("terms_and_conditions_asset_id")
  private String mTermsAndConditionsAssetId;

  @Expose
  @SerializedName("is_security_code_applicable")
  private boolean mIsSecurityCodeApplicable;

  @Expose
  @SerializedName("digitized_card_id")
  private String mDigitizedCardId;

  @Expose
  @SerializedName("payment_network")
  private String mPaymentNetwork;

  @Expose
  @SerializedName("card_digitization_time")
  private long mCardDigitizationTime;

  public String getEligibilityReceipt() {
    return mEligibilityReceipt;
  }

  public CardDto setEligibilityReceipt(String eligibilityReceipt) {
    mEligibilityReceipt = eligibilityReceipt;

    return this;
  }

  public String getTermsAndConditionsAssetId() {
    return mTermsAndConditionsAssetId;
  }

  public CardDto setTermsAndConditionsAssetId(String termsAndConditionsAssetId) {
    mTermsAndConditionsAssetId = termsAndConditionsAssetId;

    return this;
  }

  public boolean getSecurityCodeApplicable() {
    return mIsSecurityCodeApplicable;
  }

  public CardDto setSecurityCodeApplicable(boolean securityCodeApplicable) {
    mIsSecurityCodeApplicable = securityCodeApplicable;

    return this;
  }

  public String getDigitizedCardId() {
    return mDigitizedCardId;
  }

  public CardDto setDigitizedCardId(String digitizedCardId) {
    mDigitizedCardId = digitizedCardId;

    return this;
  }

  public String getPaymentNetwork() {
    return mPaymentNetwork;
  }

  public CardDto setPaymentNetwork(String paymentNetwork) {
    mPaymentNetwork = paymentNetwork;

    return this;
  }

  public long getCardDigitizationTime() {
    return mCardDigitizationTime;
  }

  public CardDto setCardDigitizationTime(long cardDigitizationTime) {
    mCardDigitizationTime = cardDigitizationTime;

    return this;
  }
}
