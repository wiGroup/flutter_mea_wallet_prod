package za.co.wigroup.spot.prod;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

import androidx.annotation.NonNull;

import com.meawallet.mtp.InitializationFailedException;
import com.meawallet.mtp.MeaAuthenticationListener;
import com.meawallet.mtp.MeaCard;
import com.meawallet.mtp.MeaCardException;
import com.meawallet.mtp.MeaCardListener;
import com.meawallet.mtp.MeaCardState;
import com.meawallet.mtp.MeaCheckedException;
import com.meawallet.mtp.MeaCompleteDigitizationListener;
import com.meawallet.mtp.MeaEligibilityReceipt;
import com.meawallet.mtp.MeaError;
import com.meawallet.mtp.MeaException;
import com.meawallet.mtp.MeaInitializeDigitizationListener;
import com.meawallet.mtp.MeaInitializeDigitizationParameters;
import com.meawallet.mtp.MeaRemotePaymentData;
import com.meawallet.mtp.MeaRemoteTransactionListener;
import com.meawallet.mtp.MeaRemoteTransactionOutcome;
import com.meawallet.mtp.MeaTokenPlatform;
import com.meawallet.mtp.NotInitializedException;
import com.meawallet.mtp.NotRegisteredException;
import com.meawallet.mtp.PaymentNetwork;
import za.co.wigroup.spot.prod.meahelpers.CardDto;
import za.co.wigroup.spot.prod.meahelpers.SharedPreferencesManager;
import za.co.wigroup.spot.prod.meahelpers.interfaces.BaseActivityInterface;
import za.co.wigroup.spot.prod.meahelpers.receivers.TransactionReceiver;
import za.co.wigroup.spot.prod.meahelpers.utils.RemotePaymentUtil;
import za.co.wigroup.spot.prod.utils.Logger;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.KEYGUARD_SERVICE;
import static za.co.wigroup.spot.prod.PluginMethods.*;
import static za.co.wigroup.spot.prod.meahelpers.card_digitization.CardEncryptionHelper.*;

/** FlutterMeaWalletProdPlugin */
public class FlutterMeaWalletProdPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware, BaseActivityInterface,  PluginRegistry.ActivityResultListener {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  static MethodChannel channel;
  static Activity activity;
  static SharedPreferencesManager mSharedPreferencesManager;

  private static final String TAG = "FLUTTERMEAWALLETPLUGIN";

  private static final int SET_AS_DEFAULT_PAYMENT_APP_REQUEST_CODE = 0;
  public static final int REQUEST_CODE_DEVICE_UNLOCK = 1001;
  public static final int REMOTE_TRANSACTION_SUCCESS = 2000;
  public static final int REMOTE_TRANSACTION_FAILURE = 2001;
  public static final int CARD_ACTIVATION_COMPLETED = 3000;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "flutter_mea_wallet_prod");
    channel.setMethodCallHandler(this);
    enableStrictModeForDebug();
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_mea_wallet_prod");
    channel.setMethodCallHandler(new FlutterMeaWalletProdPlugin());
  }

  private void enableStrictModeForDebug() {
    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
            .detectAll()
            .permitDiskReads() // disabled because a lot of disk reads done on UI thread when using LdeJsonStorage
            .permitDiskWrites() // disabled because a lot of disk writes done on UI thread when using LdeJsonStorage
            .penaltyLog()
            .build());
    StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
            .detectActivityLeaks()
            .detectFileUriExposure()
            .detectLeakedClosableObjects()
            .detectLeakedRegistrationObjects()
            .detectLeakedSqlLiteObjects()
            // can be used only on devices with Android 9 and above (a lot of warnings caused by Gson and Android Support libraries)
            //.detectNonSdkApiUsage()
            //.detectUntaggedSockets() // a lot of warnings, there is no solution how to fix for now, no impact on functionality
            //.detectCleartextNetwork() // min Android API level 23 (no warnings found)
            //.detectContentUriWithoutPermission() // min Android API level 26 (no warnings found)
            .penaltyLog()
            .build());

  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if(call.method.equals("initMeaSdk")){
      try {
        MeaTokenPlatform.initialize(activity);
        MeaTokenPlatform.registerTransactionReceiver(activity, new TransactionReceiver());
        Log.d(TAG, "MeaTokenPlatform initialized successfully");
      } catch (InitializationFailedException exception) {
        Log.d(TAG, "MeaTokenPlatform.initialize failed.", exception);
      }
    }else if (call.method.equals("registerToken")) {
      String token = call.argument("token");
      assert token != null;
      PluginMethods.registerMEAToken(token, result);
    } else if (call.method.equals("setAsDefaultPayment")) {
      if (MeaTokenPlatform.isDefaultPaymentApplication(activity)) {
        Logger.i("MEA", "Application is set as default application for contactless payments.");
      } else {
        MeaTokenPlatform.setDefaultPaymentApplication(activity, SET_AS_DEFAULT_PAYMENT_APP_REQUEST_CODE);
      }
    } else if (call.method.equals("digitizeCard")) {
      String payload = call.argument("payload");
      String cvc2 = call.argument("cvc2");
      String pubKeyMod = call.argument("pubKeyMod");
      String publicKeyFingerprint = call.argument("publicKeyFingerprint");
      String pubKeyExp = call.argument("pubKeyExp");

      assert payload != null;
      assert cvc2 != null;
      assert pubKeyMod != null;
      assert publicKeyFingerprint != null;
      assert pubKeyExp != null;

      digitizeCard(payload, pubKeyMod, publicKeyFingerprint, pubKeyExp, cvc2, result);
    }
    else if (call.method.equals("getDefaultCard")) {
     getDefaultCard(result);
    } else if (call.method.equals("deactivateCard")) {
      deactivateDefaultCard(result);
    }
    else if (call.method.equals("doContactlessPayment")) {
      doContactlessPayment(result);
    } else if (call.method.equals("cancelContactlessPayment")) {
      cancelContactlessPayment(result);
    }
    else if (call.method.equals("isNFCAvailable")) {
      isNFCAvailable(result);
    } else if (call.method.equals(("authenticateUser"))) {
      authenticateUser(result);
    } else if (call.method.equals("testRemoteTransaction")) {
      testRemoteTransaction(result);
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  void digitizeCard(String payload, String pubKeyMod, String publicKeyFingerprint, String pubKeyExp, String cvc2, MethodChannel.Result result) {
    try {
      SecretKey secretKey = generateSecretKey("AES", 256, "SHA-512");
      IvParameterSpec iv = generateIv();
      PublicKey pubKey = buildRsaPublicKey(hexStringToByteArray(pubKeyMod), hexStringToByteArray(pubKeyExp));

      byte[] encryptedData = crypt(Cipher.ENCRYPT_MODE, "AES/CBC/PKCS7Padding", null, secretKey, iv, payload.getBytes("UTF8"));
      byte[] encryptedKey = wrap("RSA/ECB/OAEPWithSHA-512AndMGF1Padding", null, pubKey, secretKey);
      byte[] initial_vector = iv.getIV();

      processEncryptedCard(encryptedData, publicKeyFingerprint, encryptedKey, initial_vector, cvc2, result);
    } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException e) {
      e.printStackTrace();
      result.error("", e.getMessage(), "");
    }
  }

  public void processEncryptedCard(byte[] encryptedCardData, String publicKeyFingerprint, byte[] encryptedKey, byte[] initialVector, String cvc2, MethodChannel.Result result) {
    String encryptedCardDataStr = bytesToHex(encryptedCardData);
    String encryptedKeyStr = bytesToHex(encryptedKey);
    String initialVectorStr = bytesToHex(initialVector);

    //Initialize the Digitization
    MeaTokenPlatform.initializeDigitization(MeaInitializeDigitizationParameters.withEncryptedPan(encryptedCardDataStr, publicKeyFingerprint, encryptedKeyStr, initialVectorStr), new MeaInitializeDigitizationListener() {
      @Override
      public void onSuccess(MeaEligibilityReceipt meaEligibilityReceipt, String termsAndConditionsAssetId, boolean isSecurityCodeApplicable) {

        try {

          MeaCard card = MeaTokenPlatform.getCardByEligibilityReceipt(meaEligibilityReceipt.getValue());
          if (card == null) {
            result.error("", "Failed to digitize card. Card is null.", "");
          }

          PaymentNetwork paymentNetwork = card.getPaymentNetwork();
          if (paymentNetwork == null) {
            result.error("", "Failed to digitize card. Payment network is null.", "");
          }

          // Cache parameters in shared preferences
          assert paymentNetwork != null;
          getSharedPreferenceManager().setCardDto(
                  new CardDto()
                          .setEligibilityReceipt(meaEligibilityReceipt.getValue())
                          .setTermsAndConditionsAssetId(termsAndConditionsAssetId)
                          .setSecurityCodeApplicable(isSecurityCodeApplicable)
                          .setPaymentNetwork(paymentNetwork.name())
                          .setCardDigitizationTime(System.currentTimeMillis()));

          //Complete the Digitization
          CardDto cardDto = getSharedPreferenceManager().getCardDtoWithEligibilityReceipt(card.getEligibilityReceipt());

          MeaTokenPlatform.completeDigitization(meaEligibilityReceipt.getValue(), termsAndConditionsAssetId, System.currentTimeMillis(), cvc2, new MeaCompleteDigitizationListener() {
            @Override
            public void onSuccess(@NonNull MeaCard meaCard) {
              getSharedPreferenceManager().updateCardDto(cardDto.setDigitizedCardId(meaCard.getId()));
              try {
                meaCard.setAsDefaultForContactlessPayments();
                result.success(true);
              } catch (NotInitializedException | NotRegisteredException | MeaCardException | MeaException e) {
                e.printStackTrace();
                result.error("", "Failed to set card as default ", null);
              }

            }

            @Override
            public void onRequireAdditionalAuthentication(@NonNull MeaCard meaCard) {
              getSharedPreferenceManager().updateCardDto(cardDto.setDigitizedCardId(card.getId()));
              result.error("", "Failed to complete digitization. RequireAdditionalAuthentication: " + card.getId(), "");
            }

            @Override
            public void onFailure(@NonNull MeaError meaError) {
              result.error(String.valueOf(meaError.getCode()), "Failed to complete digitization. Exception: " + meaError.getMessage(), "");
            }
          });


        } catch (MeaCheckedException exception) {
          result.error(String.valueOf(exception.getErrorCode()), "Failed to digitize card. Exception: " + exception.getMessage(), "");
        }
      }

      @Override
      public void onFailure(@NonNull MeaError meaError) {
        result.error(String.valueOf(meaError.getCode()), meaError.getMessage(), null);
      }
    });
  }

  private void deactivateDefaultCard(MethodChannel.Result result) {
    try {
      MeaCard meaCard = MeaTokenPlatform.getDefaultCardForContactlessPayments();


      meaCard.delete(new MeaCardListener() {
        @Override
        public void onSuccess(@NonNull MeaCard meaCard) {
          try {
            if (meaCard.getState() == MeaCardState.ACTIVE) {
              getSharedPreferenceManager().wipeCardDtoById(meaCard.getId());
            } else {
              getSharedPreferenceManager().wipeCardDtoByEligibilityReceipt(meaCard.getEligibilityReceipt());
            }
            result.success(true);
          } catch (NotInitializedException | NotRegisteredException | MeaException e) {
            e.printStackTrace();
            result.error("", "Failed to delete card: " + e.getMessage(), "");
          }
        }

        @Override
        public void onFailure(@NonNull MeaError meaError) {
          result.error("", "Failed to delete card: " + meaError.getMessage(), "");
        }
      });

    } catch (MeaCheckedException exception) {
      result.error("Error : " + exception.getErrorCode(), exception.getMessage(), null);
    }
  }

  void authenticateUser(MethodChannel.Result result) {
    try {
      MeaTokenPlatform.requestCardholderAuthentication();
    } catch (MeaCheckedException exception) {
      result.error("Error : " + exception.getErrorCode(), exception.getMessage(), null);
    }
  }

  @TargetApi(Build.VERSION_CODES.M)
  public void showDeviceUnlockScreen() {

    KeyguardManager keyguardManager = (KeyguardManager) FlutterMeaWalletProdPlugin.activity.getSystemService(KEYGUARD_SERVICE);

    if (keyguardManager != null && keyguardManager.isKeyguardSecure()) {

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && keyguardManager.isDeviceLocked()) {
        keyguardManager.requestDismissKeyguard(FlutterMeaWalletProdPlugin.activity, new KeyguardManager.KeyguardDismissCallback() {
          @Override
          public void onDismissError() {
            super.onDismissError();

            Logger.i("MainActivity", "onDismissError");
          }

          @Override
          public void onDismissSucceeded() {
            super.onDismissSucceeded();

            Logger.i("MainActivity", "onDismissSucceeded");

            try {
              MeaTokenPlatform.authenticateWithDeviceUnlock();
            } catch (MeaCheckedException exception) {
              Logger.e("MainActivity", "Failed to authenticate with device unlock.", exception);
            }
          }

          @Override
          public void onDismissCancelled() {
            super.onDismissCancelled();

            Logger.i("MainActivity", "onDismissCancelled");
          }
        });
      } else {
        Intent intent = keyguardManager.createConfirmDeviceCredentialIntent(
                "Enter credentials",
                "This needs to be done for authentication");

        FlutterMeaWalletProdPlugin.activity.startActivityForResult(intent, FlutterMeaWalletProdPlugin.REQUEST_CODE_DEVICE_UNLOCK);
      }
    } else {
      Logger.i("MainActivity", "Device lock screen is not secure.");
    }
  }

  void testRemoteTransaction(MethodChannel.Result result) {
    //Get mea card
    try {
      MeaCard meaCard = MeaTokenPlatform.getDefaultCardForContactlessPayments();

      if (meaCard != null) {
        meaCard.processRemoteTransaction(RemotePaymentUtil.getSampleMeaRemotePaymentData(20, 978), new MeaRemoteTransactionListener() {
          @Override
          public void onRemoteCompleted(@NonNull MeaCard meaCard, @NonNull MeaRemoteTransactionOutcome meaRemoteTransactionOutcome) {
            sendMessageToFlutter("TrxListenerRes", "Test merchant", REMOTE_TRANSACTION_SUCCESS, 20);
            result.success(true);
          }

          @Override
          public void onRemoteFailed(@NonNull MeaCard meaCard, @NonNull MeaError meaError) {
           sendMessageToFlutter("TrxListenerRes", "", REMOTE_TRANSACTION_FAILURE, null);
            result.success(false);
          }

          @Override
          public void onAuthenticationRequired(@NonNull MeaCard meaCard, @NonNull MeaRemotePaymentData meaRemotePaymentData) {
            result.error("", "", "");
            authenticateUser(null);
          }
        });

      } else {
        result.error("", "Failed to retrieve card for remote transaction", "");
      }
    } catch (MeaCheckedException exception) {
      result.error("Error : " + exception.getErrorCode(), exception.getMessage(), null);
    }
  }
  //To make native side send message to flutter
  public static void sendMessageToFlutter(String method, String message, int code, Integer amount) {

    Map<String, String> channelData = new HashMap<>();
    channelData.put("message", message);
    channelData.put("code", String.valueOf(code));
    channelData.put("amount", String.valueOf(amount));

    channel.invokeMethod(method, channelData, new MethodChannel.Result() {
      @Override
      public void success(Object o) {
        Log.d("CHANNEL COMM", "SUCCESS");
      }

      @Override
      public void error(String s, String s1, Object o) {
        Log.d("CHANNEL COMM", "ERROR SEND MSG");
      }

      @Override
      public void notImplemented() {
        Log.d("CHANNEL COMM", "NOT IMPLEMENTED");
      }
    });
  }

  @Override
  public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
    activity = binding.getActivity();
    MeaTokenPlatform.setAuthenticationListener(new MeaAuthenticationListener() {
      @Override
      public void onWalletPinRequired() {

      }

      @Override
      public void onCardPinRequired(@NonNull MeaCard meaCard) {

      }

      @Override
      public void onDeviceUnlockRequired() {
        showDeviceUnlockScreen();
      }

      @Override
      public void onFingerprintRequired() {

      }

      @Override
      public void onFailure(@NonNull MeaError meaError) {

      }
    });
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {

  }

  @Override
  public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {

  }

  @Override
  public void onDetachedFromActivity() {

  }

  @Override
  public Context getContext() {
    return activity.getApplicationContext();
  }

  @Override
  public Activity getActivity() {
    return activity;
  }

  @Override
  public SharedPreferencesManager getSharedPreferenceManager() {
    return (mSharedPreferencesManager == null) ?
            (mSharedPreferencesManager = new SharedPreferencesManager(activity)) : (mSharedPreferencesManager);
  }

  @Override
  public String getString(int stringResourceId) {
    return null;
  }

  @Override
  public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_CODE_DEVICE_UNLOCK) {

      switch (resultCode) {
        case RESULT_OK:
          Logger.i("MainActivity", "RESULT_OK");

          try {
            MeaTokenPlatform.authenticateWithDeviceUnlock();
          } catch (MeaCheckedException exception) {
            Logger.e("MainActivity", "Failed to authenticate with device unlock.", exception);
          }
          break;

        case RESULT_CANCELED:
          Logger.i("MainActivity", "RESULT_CANCELED");
          break;
      }

      return true;
    }
    return false;
  }

//  @Override
//  public void onWalletPinRequired() {
//
//  }
//
//  @Override
//  public void onCardPinRequired(@NonNull MeaCard meaCard) {
//
//  }
//
//  @Override
//  public void onDeviceUnlockRequired() {
//    showDeviceUnlockScreen();
//  }
//
//  @Override
//  public void onFingerprintRequired() {
//
//  }
//
//  @Override
//  public void onFailure(@NonNull MeaError meaError) {
//
//  }
}
